from django.urls import path
from .views import (
    api_list_customers,
    api_show_customers,
    api_list_salespeople,
    api_show_salespeople,
    api_list_sales,
    api_list_autos,
)

urlpatterns = [
    path("customers/create/", api_list_customers, name="create_customer"),
    path("customers/", api_list_customers, name="list_customers"),
    path("customers/<int:customer_id>/", api_show_customers, name="show_customers"),
    path("salespeople/", api_list_salespeople, name="list_salespeople"),
    path("salespeople/create/", api_list_salespeople, name="create_salespeople"),
    path(
        "salespeople/<int:employee_id>/", api_show_salespeople, name="show_salespeople"
    ),
    path("sales/", api_list_sales, name="list_sales"),
    path("sales/<int:id>/", api_list_sales, name="show_salespeople_sales"),
    path("autos/", api_list_autos, name="list_autos"),
]
