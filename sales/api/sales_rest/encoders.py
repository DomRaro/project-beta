from common.json import ModelEncoder
from .models import AutomobileVO, SaleRecord, SalesPerson, Customer


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["employee_number"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["salesperson_name", "employee_number", "id"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["customer", "sales_person", "automobile", "price", "id"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
        "sales_person": SalesPersonEncoder(),
    }
