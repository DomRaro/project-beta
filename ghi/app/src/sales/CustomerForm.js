import React, { useState } from 'react'

function CustomerForm() {

    const handleSubmit = async (e) => {

        e.preventDefault();
        const data = {};

        data.name = name;
        data.address = address;
        data.phone_number = phoneNumber;

        const url = 'http://localhost:8090/api/customers/create/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();

            setName('');
            setAddress('');
            setPhoneNumber('');
        }
    }

    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const handleNameChange = (e) => {
        setName(e.target.value)
    }

    const handleAddressChange = (e) => {
        setAddress(e.target.value)
    }

    const handlePhoneChange = (e) => {
        setPhoneNumber(e.target.value)
    }


    return (<>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Customer Registration</h1>
                    <hr />
                    <form id="create-customer-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                required
                                className="form-control"
                                name="name"
                                id="name"
                                type="text"
                                onChange={handleNameChange}
                                value={name}
                            />
                            <label htmlFor="name">Customer name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                required
                                className="form-control"
                                name="address"
                                id="address"
                                type="text"
                                onChange={handleAddressChange}
                                value={address}
                            />
                            <label htmlFor="address">Customer address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                required
                                className="form-control"
                                name="price"
                                id="price"
                                type="text"
                                onChange={handlePhoneChange}
                                value={phoneNumber}
                            />
                            <label htmlFor="sale_price">Customer phone number</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    </>
    )
}

export default CustomerForm