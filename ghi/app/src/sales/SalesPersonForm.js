import React, { useState } from 'react'

function SalesPersonForm() {

    const handleSubmit = async (e) => {

        e.preventDefault();
        const data = {};

        data.salesperson_name = salespersonName;
        data.employee_number = employeeNumber;

        const url = 'http://localhost:8090/api/salespeople/create/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();

            setSalespersonName('');
            setEmployeeNumber('');
        }
    }

    const [salespersonName, setSalespersonName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handlePersonChange = (e) => {
        setSalespersonName(e.target.value)
    }

    const handleNumberChange = (e) => {
        setEmployeeNumber(e.target.value)
    }

    return (<>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Sales Person Registration</h1>
                    <hr />
                    <form id="create-sales-person-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                required
                                className="form-control"
                                name="name"
                                id="name"
                                type="text"
                                onChange={handlePersonChange}
                                value={salespersonName}
                            />
                            <label htmlFor="name">Employee name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                required
                                className="form-control"
                                name="employee_number"
                                id="employee_number"
                                type="text"
                                onChange={handleNumberChange}
                                value={employeeNumber}
                            />
                            <label htmlFor="employee_number">Employee number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default SalesPersonForm