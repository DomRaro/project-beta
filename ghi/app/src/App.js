import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianForm from "./service/TechnicianForm";
import ServiceForm from "./service/ServiceForm";
import ServiceList from "./service/ServiceList";
import SalesList from "./sales/SalesList";
import SalesPersonList from "./sales/SalesPersonList";
import SalesForm from "./sales/SalesForm";
import CustomerForm from "./sales/CustomerForm";
import SalesPersonForm from "./sales/SalesPersonForm";
import ServiceHistory from "./service/ServiceHistory";
import ListManufacturers from "./inventory/ListManufacturers";
import ManufacturerForm from "./inventory/ManufacturerForm";
import ListModels from "./inventory/ListModels";
import ModelForm from "./inventory/ModelForm";
import AutomobileList from "./inventory/AutomobileList";
import AutomobileForm from "./inventory/AutomobileForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="automobiles/create/" element={<AutomobileForm />} />
          <Route path="automobiles/" element={<AutomobileList />} />
          <Route path="models/create/" element={<ModelForm />} />
          <Route path="models/" element={<ListModels />} />
          <Route path="manufacturers/create/" element={<ManufacturerForm />} />
          <Route path="manufacturers/" element={<ListManufacturers />} />
          <Route path="salesperson/create/" element={<SalesPersonForm />} />
          <Route path="customer/create/" element={<CustomerForm />} />
          <Route path="sales/create/" element={<SalesForm />} />
          <Route path="/sales/detail/" element={<SalesPersonList />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/" element={<MainPage />} />
          <Route path="/technician/" element={<TechnicianForm />} />
          <Route path="/service/new/" element={<ServiceForm />} />
          <Route path="/service/" element={<ServiceList />} />
          <Route path="/service/history/" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
