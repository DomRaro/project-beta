import React, { useState, useEffect } from "react";

function ServiceList() {
  const [services, setServices] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8080/api/services/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setServices(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (id) => {
    const url = `http://localhost:8080/api/services/${id}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      fetchData();
    } else {
      alert("Service was not deleted!");
    }
  };

  const handleFinish = async (id, finished) => {
    const url = `http://localhost:8080/api/services/${id}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ finished: !finished }), // toggle the value of finished
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      fetchData();
    } else {
      alert("Service was not updated!");
    }
  };

  return (
    <div className="container">
      <h1 className="mt-4 mb-4">Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Owner</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {services.map((service) => {
            if (service.finished) {
              return null;
            }
            const appointmentDate = new Date(service.appointment);
            const appointmentDateString = appointmentDate.toLocaleDateString();
            const appointmentTimeString = appointmentDate.toLocaleTimeString();
            return (
              <tr key={service.id}>
                <td className="align-middle">{service.vin}</td>
                <td className="align-middle">{service.owner}</td>
                <td className="align-middle">{appointmentDateString}</td>
                <td className="align-middle">{appointmentTimeString}</td>
                <td className="align-middle">
                  {service.technician.technician_name}
                </td>
                <td className="align-middle">{service.reason}</td>
                <td className="align-middle">{service.vip ? "👑" : "⛔"}</td>
                {/* a ternary operator, if service.vip = true return yes, else return false */}
                <td className="align-middle">
                  <div className="btn-group" role="group" aria-label="Actions">
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDelete(service.id)}
                    >
                      Cancel
                    </button>
                    <button
                      className="btn btn-success"
                      onClick={() =>
                        handleFinish(service.id, !service.finished)
                      }
                    >
                      Finished
                    </button>
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceList;
